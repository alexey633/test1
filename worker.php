<?php

class worker {

    private $age;
    private $name;

    public function __construct($age = '', $name = ''){
        if(!empty($age) && gettype($age) == 'integer') {
            $this->age = $age;
        } else {
            echo 'Некорректно передан возраст';
            return false;
        }

        if(!empty($name) && gettype($name) == 'string') {
            $this->name = $name;
        } else {
            echo 'Некорректно передано имя';
            return false;
        }
        $toFile = $this->name . ' ' . $this->age;
        file_put_contents(__DIR__ . '/test.txt', $toFile . PHP_EOL, FILE_APPEND);
    }

}

//for($i = 1; $i < 84; $i++) {
    $class = new worker('' , 'Петя');
//}
